import { Router } from "express";
import { hasMutationController, statsController } from "../controller/mutation.controller";

class MigrationRoutes {
    public router: Router;
    constructor(){
        this.router = Router();
        this.routes();
    }

    routes(): void {
        this.router.post('/mutation', hasMutationController);
        this.router.get('/stats', statsController);
        this.router.get('/', (req, res) => res.status(200).json({message: 'Version: 0.0.0'}));
    }
}

const migrationRoutes = new MigrationRoutes();
migrationRoutes.routes();

export default migrationRoutes.router;