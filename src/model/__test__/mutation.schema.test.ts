import * as db from './db'
import Mutations from '../mutation.schema'

describe('mutation schema', () => {
    beforeAll(async () => {
        await db.connect()
    })

    afterEach(async () => {
        await db.clearDatabase()
    })

    afterAll(async () => {
        await db.closeDatabase()
    })


    test('should be created correctly', (cb) => {
        const mutation = new Mutations()
        mutation.adn = ['a', 'b']
        mutation.adnHash = 'abcd'
        mutation.hasMutation = true
        mutation.save().then((result) => {
            Mutations
                .findOne({adnHash: 'abcd'})
                .exec()
                .then((mutationDB) => {
                    expect(mutationDB?.adnHash).toBe('abcd')
                    expect(mutationDB?.hasMutation).toBeTruthy()
                })
        })

        return cb()
    })
})