import { Schema, Document, Model, model } from 'mongoose'

type MutationType = MutationModel & Document

interface MutationModel {
    adn: string[],
    adnHash: string,
    hasMutation: boolean
}

const MutationSchema = new Schema({
    adn: { type: 'array', requiered: true },
    adnHash: { type: 'string', requiered: true, unique: true, index: true },
    hasMutation: { type: 'boolean', requiered: true }
})

const Mutations: Model<MutationType> = model<MutationType>('Mutations', MutationSchema, 'Mutations')

export default Mutations