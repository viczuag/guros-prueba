import * as Joi from "joi";

export const mutationSchema = Joi.array().items(Joi.string().length(6))