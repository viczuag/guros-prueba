export function checkMutation(adnString: string): boolean {
    let respuesta: number[] = []
    const arreglo = adnString.toUpperCase().split('');
    const sinDuplicados: string[] = eliminarLetrasDuplicadas(arreglo);

    try {
        respuesta = sinDuplicados.map((item) => {
            const regex = new RegExp(`${item}${item}${item}${item}`, 'g');
            const isValid = [...adnString.matchAll(regex)]
            return isValid.length;
        })
    } catch (error) {
        console.error(error)
        return false
    }

    return respuesta.includes(1)
}

export function eliminarLetrasDuplicadas(cadena: string[]): string[] {
    const dataArray = new Set(cadena);
     return [...dataArray]
}

export function horizontal(arreglo: string[]): boolean {
    const sinDuplicados: string[] = eliminarLetrasDuplicadas(arreglo)
    const evaluacion = sinDuplicados.map(checkMutation)
    return evaluacion.includes(true)
}

export function vertical(arreglo: string[]): boolean {
    const longitud = arreglo[0].length
    const newArray: string[] = []
    for(let i = 0; i < longitud; i++) {
        const newString = arreglo.reduce((sum, acc) => sum + acc[i], '')
        newArray.push(newString)
    }

    return horizontal(newArray)
}
