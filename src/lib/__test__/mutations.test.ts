import { eliminarLetrasDuplicadas, horizontal, checkMutation } from "../mutations"

describe('mutation utils', () => {
    describe('eliminarDuplicados function', () => {
        it('should receive array with string duplicates', () => {
            const unicos = eliminarLetrasDuplicadas(['abc', 'def', 'abc', 'def'])
            expect(unicos.length).toBe(2)
        })

        it('should receive array without string duplicates', () => {
            const unicos = eliminarLetrasDuplicadas(['abc', 'def', 'ghi', 'jkl'])
            expect(unicos.length).toBe(4) 
        })

        it('should receive array with letters duplicates', () => {
            const unicos = eliminarLetrasDuplicadas(['a', 'b', 'b', 'a'])
            expect(unicos.length).toBe(2)
        })

        it('should receive array without letters duplicates', () => {
            const unicos = eliminarLetrasDuplicadas(['a', 'b', 'c', 'd'])
            expect(unicos.length).toBe(4) 
        })
    })

    describe('validador function', () => {
        it('should receive a string with mutation', () => {
            const isValid = checkMutation('CCCCTA')
            expect(isValid).toBeTruthy()
        })

        it('should receive a string without mutation', () => {
            const isValid = checkMutation('CCTCTA')
            expect(isValid).toBeFalsy()
        })
    })

    describe('horizontal function', () => {
        it('should receive a array with mutation', () => {
            const arreglo = [ "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA",
            "TCACTG" ]
            const hasMutations = horizontal(arreglo)
            expect(hasMutations).toBeTruthy()
        })

        it('should receive a array without mutation', () => {
            const arreglo = [ "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CACCTA",
            "TCACTG" ]
            const hasMutations = horizontal(arreglo)
            expect(hasMutations).toBeFalsy()
        })
    })
})