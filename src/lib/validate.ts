import * as Joi from 'joi';

export function validateObject(input: any, schema: Joi.Schema): void {
    const { error } = schema.validate(input);

    if(error) {
        throw new Error(error.message);
    }
}