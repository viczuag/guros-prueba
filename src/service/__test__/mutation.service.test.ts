import { countMutations, hasMutationService } from "../mutation.service"
import * as db from '../../model/__test__/db'
import { MD5 } from "crypto-js"
import Mutations from "../../model/mutation.schema"

beforeAll(async () => {
    await db.connect()
})

afterEach(async () => {
    await db.clearDatabase()
})

afterAll(async () => {
    await db.closeDatabase()
})

describe('Mutation Service', () => {
    
    describe('hasMutationService function', () => {
        it('should be true', (done) => {
            const dna: string[] = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
            const adnHash = MD5(dna.join('')).toString();

            hasMutationService(dna).then((hasMutation) => {
                expect(hasMutation).toBeTruthy()
            })
            done()
        })

        it('should be false', (done) => {
            const dna: string[] = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CTCCTA", "TCACTG"]
            const adnHash = MD5(dna.join('')).toString();

            hasMutationService(dna).then((hasMutation) => {
                expect(hasMutation).toBeFalsy()
            })
            done()
        })
    })

    describe('countMutations function', () => {
        it('should be count correctly', () => {
            const mutation1 = new Mutations()
            mutation1.adn = ['a', 'b']
            mutation1.adnHash = 'abcd'
            mutation1.hasMutation = true
            const mutation2 = new Mutations()
            mutation2.adn = ['a', 'b']
            mutation2.adnHash = 'abcd'
            mutation2.hasMutation = false
            const mutation3 = new Mutations()
            mutation3.adn = ['a', 'b']
            mutation3.adnHash = 'abcd'
            mutation3.hasMutation = true
            mutation1.save().then(() => {
                mutation2.save().then(() => {
                    mutation3.save().then(() => {
                        countMutations().then((result) => {
                            expect(result?.count_mutations).toBe(2)
                            expect(result?.count_no_mutations).toBe(1)
                        })
                    })
                })
            })
        })
    })
})