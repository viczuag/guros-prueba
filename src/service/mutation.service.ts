import { MD5 } from "crypto-js";
import Mutations from "../model/mutation.schema";
import { horizontal, vertical } from "../lib/mutations";
import { mutationSchema } from "../schema/mutation.schema";
import { validateObject } from "../lib/validate";

export async function hasMutationService(dna: Array<string>) {

    validateObject(dna, mutationSchema)

    const hasMutation = vertical(dna) && horizontal(dna)
    const adnHash = MD5(dna.join('')).toString();

    const objeto = {
        adn: dna,
        adnHash,
        hasMutation,
    }

    await Mutations.findOneAndUpdate({ adnHash }, objeto, 
        { upsert: true, new: true, setDefaultsOnInsert: true })
        .catch(err => {
            throw new Error(err)
        })
    return hasMutation;
}

export async function countMutations() {
    const countMutations = await Mutations.count({hasMutation: true})
    const countNoMutations = await Mutations.count({hasMutation: false})
    const ratio = countMutations/countNoMutations

    return { 
        count_mutations: countMutations,
        count_no_mutations: countNoMutations,
        ratio
    }
}