import { Request, Response } from "express";
import { countMutations, hasMutationService } from "../service/mutation.service";

export async function hasMutationController(req: Request, res: Response) {
    try {
        const { body: { dna } } = req
        if (!dna) throw new Error('Invalid body')
        const hasMutation = await hasMutationService(dna)
        return res.status(hasMutation ? 200 : 403).json({message: hasMutation})
    } catch (error: any) {
        res.status(500).json({ status: 500, message: error?.message });
    }
}

export async function statsController(req: Request, res: Response) {
    try {
        const stats = await countMutations()
        return res.status(200).json({message: stats})
    } catch (error: any) {
        res.status(500).json({ status: 500, message: error?.message });
    }
}