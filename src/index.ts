import express, { Application } from 'express';
import mongoose from 'mongoose';
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv'


import { MigrationRoutes } from './routes'
import Logger from './lib/logger';

class Server {
    public app: Application;
    
    constructor() {
        dotenv.config()
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void {
        const mongodb = 'mongodb://victor:fPPJHV2pr9o0iyRG@cluster0-shard-00-00.7wjq2.mongodb.net:27017,cluster0-shard-00-01.7wjq2.mongodb.net:27017,cluster0-shard-00-02.7wjq2.mongodb.net:27017/guros?ssl=true&replicaSet=atlas-dmix2w-shard-0&authSource=admin&retryWrites=true&w=majority'
        const port = 8080
        mongoose.connect(mongodb)
            .then(data => Logger.info('conectado '))
            .catch(err => Logger.error('Error al conectar' + err))

        this.app.set("port", port)
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(morgan("dev"));
        this.app.use(cors());
    }

    routes(): void {
        this.app.use(MigrationRoutes)
    }

    start(): void {
        this.app.listen(this.app.get('port'), () => {
            Logger.info("Server on port:", this.app.get("port"));
          });
    }
}

const server = new Server();
server.start();