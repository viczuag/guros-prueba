# Prueba Guros

_Detecta si una persona tiene diferencias genéticas basándose en
su secuencia de ADN_

## Comenzando 🚀

### Pre-requisitos 📋

_Para poder ejecutar el proyecto debes tener instaladas las siguientes dependencias:_

- **NodeJS**
- **Mongo DB**
- **Typescript**

### Instalación 🔧

_Para poder levantar el servidor localmente necesitas_

1. Clonar este repositorio 
```
git clone https://gitlab.com/viczuag/guros-prueba.git
```
2. Ve a la carpeta del proyecto 
```
cd guros-prueba
```
3. Instala las dependencias 
```
npm install
```
4. Corre el proyecto en tu ambiente local 
```
npm run dev
```

_Los endpoints configurados son los siguientes:_

- **/POST - /mutation**
```
curl --location --request POST 'localhost:8080/mutation' \
--header 'Content-Type: application/json' \
--data-raw '{
    "dna": [
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCTA",
        "TCACTGG"
    ]
}'
```

- **/GET - /mutation**
```
curl --location --request GET 'localhost:8080/stats'
```

## Ejecutando las pruebas ⚙️

_Para ejecutar las pruebas del código solo tenemos que escribir el siguiente comando_

```
npm run dev
```

## Despliegue 📦

_Para hacer el despliegue solo tenemos que hacer un commit a la rama `main` de este repositorio, y se ejecuta un pipeline de gitlab, para el despliegue automatico en app engine_

La URL publica del proyecto es `https://guros-prueba.uc.r.appspot.com`
